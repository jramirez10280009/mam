/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Principal {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("muchosamuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Collection<Lineaorden> linO;
        linO = em.createNamedQuery("Lineaorden.findAll").getResultList();
        Orden o = em.find(Orden.class, 0);
        for (Lineaorden lo : linO) {
            System.out.println("Orden: " + lo.getOrden().getIdOrden());
            System.out.println("Producto: " + lo.getProducto().getIdProducto());
            System.out.println("Desc. Producto: " + lo.getProducto().getDescripcion());
            System.out.println("Precio: " + lo.getProducto().getPrecio());
            System.out.println("Cantidad: " + lo.getCantidad());
            System.out.println("Fecha: " + lo.getOrden().getFechaOrden()); 
            System.out.println("________________________________________________");
            o = lo.getOrden();
        }
        try {
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        em.close();
        emf.close();
    }
}
