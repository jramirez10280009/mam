/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package paquete;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "lineaorden")
@NamedQueries({
    @NamedQuery(name = "Lineaorden.findAll", query = "SELECT l FROM Lineaorden l")})
public class Lineaorden implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LineaordenPK lineaordenPK;
    @Column(name = "cantidad")
    private Integer cantidad;
    @JoinColumn(name = "idProducto", referencedColumnName = "idProducto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;
    @JoinColumn(name = "idOrden", referencedColumnName = "idOrden", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Orden orden;

    public Lineaorden() {
    }

    public Lineaorden(LineaordenPK lineaordenPK) {
        this.lineaordenPK = lineaordenPK;
    }

    public Lineaorden(int idOrden, int idProducto) {
        this.lineaordenPK = new LineaordenPK(idOrden, idProducto);
    }

    public LineaordenPK getLineaordenPK() {
        return lineaordenPK;
    }

    public void setLineaordenPK(LineaordenPK lineaordenPK) {
        this.lineaordenPK = lineaordenPK;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lineaordenPK != null ? lineaordenPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lineaorden)) {
            return false;
        }
        Lineaorden other = (Lineaorden) object;
        if ((this.lineaordenPK == null && other.lineaordenPK != null) || (this.lineaordenPK != null && !this.lineaordenPK.equals(other.lineaordenPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "paquete.Lineaorden[ lineaordenPK=" + lineaordenPK + " ]";
    }
    
}
